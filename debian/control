Source: libgraph-readwrite-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: Florian Schlichting <fsfs@debian.org>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 13)
Build-Depends-Indep: libgraph-perl <!nocheck>,
                     libparse-yapp-perl <!nocheck>,
                     libxml-parser-perl <!nocheck>,
                     libxml-writer-perl <!nocheck>,
                     perl
Standards-Version: 4.6.0
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libgraph-readwrite-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libgraph-readwrite-perl.git
Homepage: https://metacpan.org/release/Graph-ReadWrite
Rules-Requires-Root: no

Package: libgraph-readwrite-perl
Architecture: all
Depends: ${misc:Depends},
         ${perl:Depends},
         libgraph-perl,
         libio-all-perl,
         libparse-yapp-perl,
         libxml-parser-perl,
         libxml-writer-perl
Description: module for reading and writing directed graphs
 Graph::ReadWrite is a collection of perl classes for reading and writing
 directed graphs in a variety of file formats. The graphs are represented in
 Perl using Jarkko Hietaniemi's Graph classes.
 .
 There are two base classes, Graph::Reader which is the Base class for classes
 which read a graph file and create an instance of the Graph class, and
 Graph::Writer which is the Base class for classes which take an instance of
 the Graph class and write it out in a specific file format.
 .
 Supported formats: XML, Dot, VCG, daVinci, HTK
